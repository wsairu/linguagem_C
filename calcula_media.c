#include <stdio.h>

int main(){

	float nota[4];
	int i = 0;
	char bimestre[4][9] = {
		{"Primeiro\0"},
		{"Segundo\0"},
		{"Terceiro\0"},
		{"Quarto\0"}
	};

	float media = 0;
	char nome[26];

	printf("Nome do Aluno: ");
	scanf("%25[^\n]s", nome);

	for(i=0;i<4;i++){
		printf("%s %s", bimestre[i] ,"bimestre: ");
		scanf("%f", &nota[i]);
		media += nota[i];
	}

	printf("\n****************************");

	printf("\nMédia do aluno %s: %.2f\n", nome, media / 4);
	
	printf("****************************\n");

	return 0;
}