#include <stdio.h>

void main(){
    //atribuicao literal
    int matriz[2][3] = {
        {3,5,6},
        {7,2,9}
    };    
     // Percorrendo todos os elementos da matriz
     for (int i = 0; i < 2; i++) { // Loop para as linhas
        for (int j = 0; j < 3; j++) { // Loop para as colunas
            printf("%d ", matriz[i][j]); // Acessando o elemento na linha i e coluna j
        }
        printf("\n"); // Nova linha após cada linha da matriz

    }
}